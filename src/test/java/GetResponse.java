
public class GetResponse {

	public static Pair getResponse(String url, String key, String cmd, int form, String var1, String var2, String var3,
			String var4, String var5, String var6, int var7, int var8) {

		if (cmd.equals("save_user_card")) {
			
			return TestSave.save(url, cmd, form, key, var1, var2, var3, var4, var5, var6, var7, var8);
		} 
		else if (cmd.equals("get_user_cards")) {
			
			return TestGet.get(url, cmd, form, key, var1);
		} 
		else {

			return TestDelete.delete(url, cmd, form, key, var1);
		}
	}

}
