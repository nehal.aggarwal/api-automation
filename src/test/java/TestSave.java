import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class TestSave {

	public static Pair save(String url, String cmd, int form, String key, String var1, String var2, String var3,
			String var4, String var5, String var6, int var7, int var8) {
		
		String input = key + "|" + cmd + "|" + var1 + "|1b1b0";
		String hash_value = Helper.hash(input);
		
		Response response = RestAssured.given().param("form", form).param("key", key).param("command", cmd)
				.param("hash", hash_value).param("var1", var1).param("var2", var2).param("var3", var3)
				.param("var4", var4).param("var5", var5).param("var6", var6).param("var7", var7).param("var8", var8)
				.post(url).then().statusCode(200).extract().response();
		
		JsonPath json = response.jsonPath();
		String token = json.getString("cardToken");
		Pair pair = new Pair();
		pair.response = response;
		pair.token = token;

		return pair;

	}

}
