import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class Testing {

	String url = "";
	String key = "";
	String command_name[] = new String[100];
	int form[] = new int[10];
	int status[] = new int[100];
	String var1[] = new String[100];
	String var2[] = new String[100];
	String var3[] = new String[100];
	String var4[] = new String[100];
	String var5[] = new String[100];
	String var6[] = new String[100];
	int var7[] = new int[100];
	int var8[] = new int[100];
	int rs, re;

	@BeforeTest
	public void data() throws EncryptedDocumentException, IOException {
		ReadExcel r = new ReadExcel();
		url = r.getUrl();
		key = r.getKey();
		command_name = r.getCommands();
		form = r.getForm();
		status = r.getStatus();
		var1 = r.getStringVar(5);
		var2 = r.getStringVar(6);
		var3 = r.getStringVar(7);
		var4 = r.getStringVar(8);
		var5 = r.getStringVar(9);
		var6 = r.getStringVar(10);
		var7 = r.getIntVar(11);
		var8 = r.getIntVar(12);
		rs = r.getRowStart();
		re = r.getRowEnd();

	}

	@Test(priority = 0, description = "Verify whether card is saved or not")
	void verifySaveMethod() {

		Pair res = GetResponse.getResponse(url, key, command_name[1], form[1], var1[1], var2[1], var3[1], var4[1],
				var5[1], var6[1], var7[1], var8[1]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[1], actual);

	}

	@Test(priority = 1, description = "Verify whether card is fetched or not")
	void verifyGetMethod() {

		Pair res = GetResponse.getResponse(url, key, command_name[2], form[2], var1[2], var2[2], var3[2], var4[2],
				var5[2], var6[2], var7[2], var8[2]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[2], actual);

	}

	@Test(priority = 2, description = "Verify whether card is deleted or not")
	void verifyDeleteMethod() {

		Pair res = GetResponse.getResponse(url, key, command_name[3], form[3], var1[3], var2[3], var3[3], var4[3],
				var5[3], var6[3], var7[3], var8[3]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[3], actual);

	}

	@Test(priority = 3, description = "Verify whether card can be fetched after deleting")
	void verifyGetMethod_forCardNotExisting() {

		Pair res = GetResponse.getResponse(url, key, command_name[4], form[4], var1[4], var2[4], var3[4], var4[4],
				var5[4], var6[4], var7[4], var8[4]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[4], actual);

	}

	@Test(priority = 4, description = "Verify whether card can be deleted again")
	void verifyDeleteMethod_forCardNotExisting() {

		Pair res = GetResponse.getResponse(url, key, command_name[5], form[5], var1[5], var2[5], var3[5], var4[5],
				var5[5], var6[5], var7[5], var8[5]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[5], actual);

	}

	@Test(priority = 5, description = "Verify whether card with wrong details can be saved or not")
	void verifySaveMethod_forWrongDetails() {

		Pair res = GetResponse.getResponse(url, key, command_name[6], form[6], var1[6], var2[6], var3[6], var4[6],
				var5[6], var6[6], var7[6], var8[6]);
		JsonPath result = res.response.jsonPath();
		int actual = result.get("status");
		String msg = result.getString("msg");
		System.out.println(msg);
		Assert.assertEquals(status[6], actual);

	}

	@Test(priority = 6)
	void verifyHeader() {

		for (int i = rs + 1; i <= re; ++i) {
			Pair res = GetResponse.getResponse(url, key, command_name[i], form[i], var1[i], var2[i], var3[i], var4[i],
					var5[i], var6[i], var7[i], var8[i]);
			String contentType = res.response.header("Content-Type");
			Assert.assertEquals("text/html; charset=UTF-8", contentType);
			String serverType = res.response.header("Server");
			Assert.assertEquals("Apache", serverType);

		}

	}

}
