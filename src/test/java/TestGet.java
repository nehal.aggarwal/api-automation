import java.util.LinkedHashMap;

import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class TestGet {
	public static Pair get(String url, String cmd, int form, String key, String var1) {

		String input = key + "|" + cmd + "|" + var1 + "|1b1b0";

		String hash_value = "";
		hash_value += Helper.hash(input);
		String hash_value1 = "0" + hash_value;

		Response response = RestAssured.given().param("form", form).param("key", key).param("command", cmd)
				.param("hash", hash_value1).param("var1", var1).post(url).then().statusCode(200).extract().response();

		JsonPath json = response.jsonPath();
		String token = "";
		String msg = json.getString("msg");
		Pair pair = new Pair();

		if (msg.equals("Card not found.")) {
			pair.token = "Token not found";
			pair.response = response;
			return pair;
		}

		Map<String, Map<String, String>> val = new LinkedHashMap<String, Map<String, String>>();
		val = json.get("user_cards");
		Map.Entry<String, Map<String, String>> entry = val.entrySet().iterator().next();
		token = entry.getKey();

		pair.response = response;
		pair.token = token;

		return pair;

	}

}
