import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TestDelete {
	public static Pair delete(String url, String cmd, int form, String key, String var1) {
		
		String input = key + "|" + cmd + "|" + var1 + "|1b1b0";
		String hash_value = Helper.hash(input);
		Pair get_res = TestGet.get(url, "get_user_cards", form, key, var1);
		String token = get_res.token;
		Response response = RestAssured.given().param("form", form).param("key", key).param("command", cmd)
				.param("hash", hash_value).param("var1", var1).param("var2", token).post(url).then().statusCode(200)
				.extract().response();

		Pair pair = new Pair();
		pair.response = response;
		pair.token = "";
		return pair;

	}

}
