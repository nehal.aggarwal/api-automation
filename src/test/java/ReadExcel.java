import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ReadExcel {

	public int getRowStart() throws IOException {

		int rowstart = 0;
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelSheetNehal-3.xlsx").getFile());
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheetAt(0);
		rowstart = sheet.getFirstRowNum();
		file.close();

		return rowstart;
	}

	public int getRowEnd() throws IOException {

		int rowend = 0;
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelSheetNehal-3.xlsx").getFile());
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheetAt(0);
		rowend = sheet.getLastRowNum();
		file.close();
		return rowend;
	}

	public int[] getForm() throws IOException {

		int form[] = new int[10];
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelSheetNehal-3.xlsx").getFile());
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheetAt(0);
		int rowStart = sheet.getFirstRowNum();
		int rowEnd = sheet.getLastRowNum();
		for (int i = rowStart + 1; i <= rowEnd; i++) {
			Row row = sheet.getRow(i);
			form[i] = (int) row.getCell(3).getNumericCellValue();
		}
		file.close();
		return form;
	}

	public String[] getCommands() throws IOException {

		String cmd[] = new String[10];
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelSheetNehal-3.xlsx").getFile());
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheetAt(0);
		int rowStart = sheet.getFirstRowNum();
		int rowEnd = sheet.getLastRowNum();
		for (int i = rowStart + 1; i <= rowEnd; i++) {
			Row row = sheet.getRow(i);
			cmd[i] = row.getCell(1).getStringCellValue();
		}
		file.close();
		return cmd;
	}

	public String[] getStringVar(int index) throws IOException {

		String var[] = new String[10];
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelSheetNehal-3.xlsx").getFile());
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheetAt(0);
		int rowStart = sheet.getFirstRowNum();
		int rowEnd = sheet.getLastRowNum();
		for (int i = rowStart + 1; i <= rowEnd; i++) {
			Row row = sheet.getRow(i);
			var[i] = row.getCell(index).getStringCellValue();
		}
		file.close();
		return var;
	}

	public int[] getIntVar(int index) throws IOException {

		int var[] = new int[10];
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelSheetNehal-3.xlsx").getFile());
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheetAt(0);
		int rowStart = sheet.getFirstRowNum();
		int rowEnd = sheet.getLastRowNum();
		for (int i = rowStart + 1; i <= rowEnd; i++) {
			Row row = sheet.getRow(i);
			var[i] = (int) row.getCell(index).getNumericCellValue();
		}
		file.close();
		return var;
	}

	public String getUrl() throws IOException {

		String url = "";
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelSheetNehal-3.xlsx").getFile());
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheetAt(0);
		url = sheet.getRow(1).getCell(0).getStringCellValue();
		file.close();
		return url;
	}

	public String getKey() throws IOException {

		String key = "";
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelSheetNehal-3.xlsx").getFile());
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheetAt(0);
		key = sheet.getRow(1).getCell(4).getStringCellValue();
		file.close();
		return key;
	}

	public int[] getStatus() throws IOException {

		int status[] = new int[100];
		ClassLoader classLoader = this.getClass().getClassLoader();
		FileInputStream file = new FileInputStream(classLoader.getResource("ExcelSheetNehal-3.xlsx").getFile());
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheetAt(0);
		int rowStart = sheet.getFirstRowNum();
		int rowEnd = sheet.getLastRowNum();
		for (int i = rowStart + 1; i <= rowEnd; i++) {
			Row row = sheet.getRow(i);
			status[i] = (int) row.getCell(2).getNumericCellValue();
		}
		file.close();
		return status;
	}

}
